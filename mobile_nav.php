<nav role="navigation">
  <ul>
    <li><a class="mob" href="#home">Home</a></li>
    <li><a class="mob" href="#skills">Skills</a></li>
    <li><a class="mob" href="#work">Recent Work</a></li>
    <li><a class="mob" href="#about">About</a></li>
    <li><a class="mob" href="#contact">Contact Me</a></li>
  </ul>
</nav>