This is a complete overhaul of my personal portfolio site: 

        http://www.bkevans.com

Sass used as a CSS preprocessor; grunt used to compile and minify CSS as well as to uglify Javascript. PHP is used throughout, localhost used for development.

        http://gruntjs.com
        https://nodejs.org/en/

I chose not to use a framework for this site, as I am enjoying custom building it from scratch.

Working Sass/JS files are located in the "Assets" folder.

Production Version CSS/JS files are located in the "Build" folder.

Images are in the img folder.

Font awesome icons are used in this build; they are located in the fonts folder, and the ones being used can be found at the bottom of the master.scss file.

        https://fortawesome.github.io/Font-Awesome/
        https://daneden.github.io/animate.css/

Normalize.scss resets css settings. Animate.scss is in the assets folder, and only has the animations being used by the site. Both of these files are minified into CSS versions located in the Build folder