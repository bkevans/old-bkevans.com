<nav role="navigation">
  <ul id="navUl">
    <li class="wow fadeIn" data-wow-delay="1.1s"><a href="#skills">Skills</a></li>
    <li class="wow fadeIn" data-wow-delay="1.4s"><a href="#work">Recent Work</a></li>
    <li class="wow fadeIn" data-wow-delay="1.7s"><a href="#about">About</a></li>
    <li class="wow fadeIn" data-wow-delay="2s"><a href="#contact">Contact Me</a></li>
  </ul>
</nav>