		//Mobile nav dropdown
		$(function() {
			$('#mobile').hide();
			$('#mblIcon').add('.mob').click(function(){
				$('#mobile').slideToggle("fast");
				$('#hamb').toggleClass('active');
			});
		});
		
		//Hide mobile nav on screen resize
		$(window).resize(function() {
				if (winWidth > 640) {
				$('#mobile').hide();}
		});

		$(function() {
			$('a').smoothScroll({
				speed: 500,
				exclude: ["#hamb"],
				updateURL: false,
			});
		});
		
		$(function() {
		$("#darrow img").on({
			 "mouseover" : function() {
			    this.src = 'img/darrow_hvr.svg';
			  },
			  "mouseout" : function() {
			    this.src= 'img/darrow.svg';
			  }
			});
		});

		$(function() {
		$("#uparrow img").on({
			 "mouseover" : function() {
			    this.src = 'img/uparrow_hvr.svg';
			  },
			  "mouseout" : function() {
			    this.src= 'img/uparrow.svg';
			  }
			});
		});

		//Form validation fun stuff
		var input;

		$('#name').on('input', function() {
			input = $(this);
			label = $(input).siblings('label').text();
			var is_name = input.val().length;
			if (is_name > 3) {
		        input.next().removeClass('exclamation-triangle').addClass('check-square-o');
		    }
		 	$(input).blur(function(){
		    	if (is_name <= 3) {
		    		input.removeClass("valid").addClass("invalid");
		    		input.next().removeClass('check-square-o').addClass('exclamation-triangle');
		    	}
			    else {
			    	input.removeClass("invalid").addClass("valid");
			        input.next().removeClass('exclamation-triangle').addClass('check-square-o');
			    }
		  	});
		});

		$('#email').on('input', function() {
			input = $(this);
			var re = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
			var is_email = re.test(input.val());
			if (is_email) {
		        input.next().removeClass('exclamation-triangle').addClass('check-square-o');
		    }
		 	$(input).blur(function(){
		    	if (!is_email) {
		    		input.removeClass("valid").addClass("invalid");
		    		input.next().removeClass('check-square-o').addClass('exclamation-triangle') 
		    	}
			    else {
			    	input.removeClass("invalid").addClass("valid");
			        input.next().removeClass('exclamation-triangle').addClass('check-square-o');
			    }
		  	});
		});

		$(function(){
		    var num1 = $('#num1');
		    var num2 = $('#num2');
		    num1.html(Math.floor(Math.random()*10+1));
		    num2.html(Math.floor(Math.random()*10+1));
		});
		
		$('#mathTest').on('input', function() {
			input = $(this);
        	var answer = parseInt($(this).val(), 10);		    
        	var num1 = parseInt($("#num1").html(), 10);
		    var num2 = parseInt($("#num2").html(), 10);
			if (answer === num1 + num2) {
		        input.next().removeClass('exclamation-triangle').addClass('check-square-o');
		    }
		 	$(input).blur(function(){
		    	if (answer != num1 + num2) {
		    		input.removeClass("valid").addClass("invalid");
		    		input.next().removeClass('check-square-o').addClass('exclamation-triangle') 
		    	}
			    else {
			    	input.removeClass("invalid").addClass("valid");
			        input.next().removeClass('exclamation-triangle').addClass('check-square-o');
			    }
		  	});
		});

		$('#message').keyup(function(event) {
			input = $(this);
			var message = $(this).val().length;
			if (message > 0) {
		        input.next().removeClass('exclamation-triangle').addClass('check-square-o');
		    }
		 	$(input).blur(function(){
		    	if (message == 0) {
		    		input.removeClass("valid").addClass("invalid");
		    		input.next().removeClass('check-square-o').addClass('exclamation-triangle') 
		    	}
			    else {
			    	input.removeClass("invalid").addClass("valid");
			        input.next().removeClass('exclamation-triangle').addClass('check-square-o');
			    }
		  	});
		});

		$("#submitBtn button").click(function(event){
			var form_data = $("#contactForm").serializeArray();
			var error_free = true;
				for (var input in form_data) {
				var element = $("#"+form_data[input]['name']);
				var valid = element.hasClass("valid");
				var error_element = $(element.next());
				if (!valid) {
					error_element.addClass("exclamation-triangle"); 
					error_free=false;
				}
				else {
					error_element.removeClass("exclamation-triangle");
				}
			}
			if (!error_free){
				$(this).removeClass().addClass("subError");
				event.preventDefault(); 
			}
			else{
				event.preventDefault(); 
				$('#submitBtn').hide();
				$('.inputs').hide();
				$('.loader').fadeIn();
				$.ajax({
				        type: "POST",
				        url: "mail_form.php",
				        data: form_data,
				        success: function(){
					        console.log(form_data);
					        $('.loader').delay(2500).fadeOut(100, function(){
			   				$('.btnDone').fadeIn();
							$('.msgPrompt').text("Thank you for your message!");
				        });
				    }	
				});		
			}
			window.setTimeout(function(){
				$('button').removeClass();
				}, 3000); 
		});
