<nav role="navigation">
  <ul>
    <li><a href="#home">Home</a></li>
    <li><a href="#about">Profile</a></li>
    <li><a href="#education">Education</a></li>
    <li><a href="#work">Recent Work</a></li>
    <li><a href="#contact">Contact Me</a></li>
  </ul>
</nav>