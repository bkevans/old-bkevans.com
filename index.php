<!DOCTYPE html>
<html prefix="og: http://ogp.me/ns#">
	<head>
		<title>Benjamin Evans - UI/UX Developer</title>
	    <meta charset="utf-8" />
	    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
	    <meta name="robots" content="INDEX,FOLLOW" />
	    <meta name="keywords" content="ui / ux, web developer, website designer, front-end developer, wilmington, jquery, UX designer, web design, graphic design, NC" />
    	<meta name="description" content="The portfolio of Wilmington, NC based UI/UX designer &amp; front-end developer Benjamin Evans." />
		<meta property="og:title" content="Benjamin Evans - Portfolio" />
		<meta property="og:url" content="http://www.bkevans.com" />
		<meta property="og:image" content="http://www.bkevans.com/img/sitepreview.jpg" />
		<link rel="stylesheet" type="text/css" href="build/css/master.css">
		<link rel="stylesheet" type="text/css" href="build/css/animate.min.css">
		<link rel="stylesheet" type="text/css" href="build/css/magnific-popup.css">
		<link href='https://fonts.googleapis.com/css?family=Julius+Sans+One' rel='stylesheet' type='text/css'>
		<link rel="icon" type="image/png" href="img/big-e.ico" sizes="16x16">		
	</head>

	<body>

	<?php include_once("analyticstracking.php") ?>

	<section id="home">
		<div id="mblIcon" class="wow fadeIn" data-wow-delay="1s"><a id="hamb" href="#menu"></a></div>
		<div id="mobile">
			<?php include ('mobile_nav.php'); ?>
		</div>
		
		<div class="topConWrap">
			<div class="fadingContent">
				<section class="wow flipInX" data-wow-delay=".2s">
	  				<h1 class="fade">BenjaminEvans</h1>
	  				<h2 class="text fade">UI / UX Designer & <br/>Front-End Developer.</h2>
	  			</section>
			</div>
		</div>

		<div id="bottomHalf">
		<div id="top-nav">	
			<?php include ('top_nav.php'); ?>
		</div>
			<a id="darrow" class="fade wow bounceIn" data-wow-delay="2.2s" href="#skills">
				<img src="img/darrow.svg"> 
			 </a>	
		</div>
		</div>
			<a id="uparrow" class="hidden" href="#home">
				<img src="img/uparrow.svg"> 
			</a>	
		</div>
	</section>

	<div class="pageWrap">
		<section id="skills">
			<div class="wrap">
				<div class="third wow fadeInLeft" data-wow-offset="180">
					<img class="skills" src="img/analytics.svg">
					<p>I decipher analytics and business trends to help establish best practices. I make sites easy to find and intuitive to use through proper SEO, usability testing, and a strong working knowledge of modern UI / UX techniques and cross-browser functionality.</p>					
				</div>
				<div class="third wow fadeIn" data-wow-offset="220"  data-wow-duration="2s">
					<img class="skills" src="img/responsive.svg">
					<p>Devices come in a dizzying array of shapes and sizes, and an ever increasing number of internet users are using mobile devices. I specialize in responsive design that adapts to different screen resolutions so that my projects always look as intended; no matter the device.</p>										
				</div>
				<div class="third wow fadeInRight" data-wow-offset="180">
					<img class="skills" src="img/code.svg">
					<p>I write clean, semantic code that conforms to the latest W3C standards. I live in Sublime text throughout my coding process, using grunt to convert Sass to CSS, and to minify my CSS and javascript files. I leverage jQuery and CSS animations to bring projects to life.</p>					
				</div>
			</div>
		</section>
	</div>
		<div class="clearfix"></div>

		<div id="work">
			<p><i class="fa quote-left"></i> Design is not just what it looks like and feels like. Design is how it works. <i class="fa quote-right"></i><br/><span>- Steve Jobs</span></p>
			<h3 class="line wow fadeIn" data-wow-offset="220" data-wow-duration="2s"><span>A few recent projects </span></h3>
		</div>
	

	<div class="pageWrap">
		<section id="portfolio" class="wrapp">
			<figure class="thirdp wow fadeInLeft" data-wow-offset="250">
				<a href="#">
					<div class="portImg"><img src="img/lobp_ss.jpg"></div>
					<div class="portOverlay"></div>
					<figcaption>
						<div class="captionWrap">
							<h3>Live Oak Bank Portal</h3>
				          	<div class="divider"><span class="inputBar"></span></div>
				          	<p>&nbsp;<!--View this project--></p>
				        </div>
					</figcaption>
				</a>
			</figure>
			<figure class="thirdp wow fade2 fadeMobR" data-wow-offset="250">
				<a class="project_view" href="projects/jea_project.php">
					<div class="portImg"><img src="img/jea_ss.jpg"></div>
					<div class="portOverlay"></div>
					<figcaption>
						<div class="captionWrap">
							<h3>Josh Evans Art</h3>
				          	<div class="divider"><span class="inputBar"></span></div>
				          	<p>View this project</p>
				        </div>
					</figcaption>
				</a>
			</figure>
			<figure class="thirdp wow fade3 fadeMobL" data-wow-offset="250">
				<a class="project_view" href="projects/tfg_project.php">
					<div class="portImg"><img src="img/tf_ss.jpg"></div>
					<div class="portOverlay"></div>
					<figcaption>
						<div class="captionWrap">
							<h3>Toyota Fiberglass</h3>
				          	<div class="divider"><span class="inputBar"></span></div>
				          	<p>View this project</p>
				        </div>
					</figcaption>
				</a>
			</figure>
			<figure class="thirdp wow fade4 fadeMobR clrLft" data-wow-offset="250">
				<a class="project_view" href="projects/lobsg_project.php">
					<div class="portImg"><img src="img/lobsg_ss.jpg"></div>
					<div class="portOverlay"></div>
					<figcaption>
						<div class="captionWrap">
							<h3>Live Oak Style Guide</h3>
				          	<div class="divider"><span class="inputBar"></span></div>
				          	<p>View this project</p>
				        </div>
					</figcaption>
				</a>
			</figure>
			<figure class="thirdp wow fade5 fadeMobL" data-wow-offset="250">
				<a href="#">
					<div class="portImg"><img src="img/idb_ss.jpg"></div>
					<div class="portOverlay"></div>
					<figcaption>
						<div class="captionWrap">
							<h3>Idea Bank Mockup</h3>
				          	<div class="divider"><span class="inputBar"></span></div>
				          	<p>&nbsp;<!--View this project--></p>
				        </div>
					</figcaption>
				</a>
			</figure>
			<figure class="thirdp wow fadeInRight" data-wow-offset="250">
				<a class="project_view" href="projects/jksurf_project.php">
					<div class="portImg"><img src="img/jk_ss.jpg"></div>
					<div class="portOverlay"></div>
					<figcaption>
						<div class="captionWrap">
							<h3>Jimmy Keith Surfboards</h3>
				          	<div class="divider"><span class="inputBar"></span></div>
				          	<p>View this project</p>
				        </div>
					</figcaption>
				</a>
			</figure>
		</section>
		</div>

		<div class="clearfix"></div>
		<section id="about" class="wrap">	
			<figure class="selfRound wow bounceIn" data-wow-offset="290"><img src="img/selfRound.png"></figure>
			<p class="wow fadeIn" data-wow-offset="290">My name is Benjamin Evans and I love creating things. All sorts of things really, from websites to surfboards to metal fabrication to lasagna...I just love the feeling of accomplishment that creating something brings. I'm currently located in Wilmington, North Carolina working as a UI / UX developer for Live Oak Bank, with an emphasis on user interaction. I am open to freelance work: send me a message using the form below or at ben@bkevans.com to chat about opportunities. Make sure to connect with me on my social media and project channels! </p>
			<ul class="social">
				<li class="wow fadeIn" data-wow-delay=".3s"><a href="http://www.codepen.io/bkevans/" title="CodePen" target="blank"><i class="fa codepen"></i></a></li>
				<li class="wow fadeIn" data-wow-delay=".5s"><a href="https://bitbucket.org/bkevans/" title="Bitbucket" target="blank"><i class="fa bitbucket"></i></a></li>
				<li class="wow fadeIn" data-wow-delay=".7s"><a href="https://www.linkedin.com/in/benkevans" title="LinkedIn" target="blank"><i class="fa linkedin"></i></a></li>
				<li class="wow fadeIn" data-wow-delay=".9s"><a href="https://www.facebook.com/ben.evans.3194" title="Facebook" target="blank"><i class="fa facebook"></i></a></li>
				<li class="wow fadeIn" data-wow-delay="1.1s"><a href="https://instagram.com/b.k.evans/" title="Instagram" target="blank"><i class="fa instagram"></i></a></li>
			</ul>
		</section>
	<div class="pageWrap">
		<section id="contact">
		<p class="text wow fadeIn msgPrompt" data-wow-offset="220"  data-wow-duration="2s">Get in touch with me.</p>
			<div class="formWrap">
                <form action="mail_form.php" id="contactForm" class="required-form" method="post" autocomplete="off">
                    <div class="inputs">
                        <input type="text" name="name" id="name" required/><i class="formIcon fa"></i>
                        <span class="inputBar"></span>
                        <label for="name"><i class="fa user"></i> Name</label>
                    </div>
                    <div class="inputs">
                        <input type="text" name="email" id="email" required/><i class="formIcon fa"></i>
                        <span class="inputBar"></span>
                        <label for="email"><i class="fa envelope-o"></i> Email</label>
                    </div>
                    <div class="inputs">
                        <input type="text" name="mathTest" id="mathTest" required/><i class="formIcon fa"></i>
                        <span class="inputBar"></span>
                       	<label for="mathTest"><i class="fa question-circle">&nbsp;</i><span id="num1"></span> + <span id="num2"></span> =</label>
                    </div>
                    <div class="inputs">
                        <textarea name="message" id="message" required></textarea><i class="formIcon fa"></i>
                        <span class="inputBar"></span>
                        <label for="message"><i class="fa comment-o"></i> Your message</label>
                    </div>
                    <div id="submitBtn">
                        <button type="submit" value="submit" data-error="Oops!"><span>Submit</span>
	                        <i class="btnStatic fa paper-plane-o fa-lg"></i>
  						</button>
                    </div>
                    	<aside class="loader"></aside>
	                    <i class="btnDone fa check-square-o"></i>
                </form>
 			</div>
		</section>
	</div>

		<script src="js/jquery-v2_1_4.js"></script>	
		<script type="text/javascript" src="build/js/main.js"></script>	
		<script type="text/javascript" src="build/js/smoothscroll.min.js"></script>	
		<script type="text/javascript" src="build/js/wow.min.js"></script>	
		<script type="text/javascript" src="build/js/magnific-popup.min.js"></script>	

		<script type="text/javascript">

			new WOW().init();

		var timer;
		var winWidth = $(window).width();
		var navTop = $("#top-nav").offset().top;
		var skillTop = $("#skills").offset().top;
		
		$(window).on('scroll', function() {
		    if(timer) {
		        window.clearTimeout(timer);
		    }
		    timer = window.setTimeout(function() {
		        var yepper = $(document).scrollTop();
				if ((yepper > navTop) && (winWidth > 640)){
					$('#top-nav').addClass('sticky');
					$('#navUl').children().css('visibility','inherit');
					$('#uparrow').fadeIn(600).removeClass('hidden');
					$('#darrow').fadeOut(600).addClass('hidden');
				}
				else {
					$('#top-nav').removeClass('sticky');
					$('#uparrow').fadeOut(400).addClass('hidden');
				}
		    }, 10);
		});

		//Fades the main logo and down arrow on scroll
		$(window).scroll(function(){
    		var top = ($(window).scrollTop() > 15) ? $(window).scrollTop() : 1;
				if (top > skillTop) {
					$('.fadingContent').hide();
				}
				else {
					$('.fadingContent').show();
	    			$('.fade').stop(true, true).fadeTo(0, 8 / top);
					$('fade').css('top', top * .35);
				} 
		});

		$('.project_view').magnificPopup({
          type: 'ajax',
          alignTop: true,
          overflowY: 'scroll' // as we know that popup content is tall we set scroll overflow by default to avoid jump
        });

		</script>

	</body>
</html>










