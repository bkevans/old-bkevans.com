<div class="popup_project wow zoomIn">
	<img src="../img/tfg_mockup.jpg" />
    <h1>Toyota Fiberglass</h1>
    <h2>Product Showcase | Site Concept</h2>
	    <div class="btnWrpr">
		    <a href="https://bitbucket.org/bkevans/toyota-fiberglass" class="prjctBtnLft" target="blank" title="Project Source Files">View Source</a>
	    </div>
	    <div class="btnWrpr">
		    <a href="http://www.tf.bkevans.com" class="prjctBtnRgt" target="blank" title="Toyota Fiberglass">Launch Site</a>
	    </div>
    <p>I developed this site as a concept for Toyota Fiberglass, makers of aftermarket fiberglass body panels for early Toyota trucks. The current look and feel of the Toyota Fiberglass website at toyotafiberglass.com does not reflect the quality of the parts these guys make. With this in mind I set out to build a simple, clean, updated concept site for Toyota Fiberglass to potentially use as a showcase of their aftermarket parts offering. This site concept is fully responsive in order to be mobile and touch device friendly.</p>
   	<p>I wrote the HTML, Sass, and Javascript for this site in Sublime Text 3, using LibSass and Grunt to compile my minified CSS and JS files. Image sizing was done in Photoshop CC, and I built updated versions of current logos in an SVG format using Illustrator CC. Git was used for version control via terminal, connecting with a bitbucket repository for storage redundancy and history tracking.</p>
	<div class="prjctImgWrap"><img src="../img/project_img/tfg_home.jpg" /></div>
	<div class="prjctImgWrap"><img src="../img/project_img/tfg_84_89.jpg" /></div>
	<div class="prjctImgWrap"><img src="../img/project_img/tfg_gallery.jpg" /></div>
</div>