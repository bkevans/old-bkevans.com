<div class="popup_project wow zoomIn">
	<img src="../img/jk_mockup.jpg" />
    <h1>Jimmy Keith Surfboards</h1>
    <h2>Web Site | Responsive Design</h2>
    <h2>Design/Development in Progress</h2>
	    <!-- <div class="btnWrpr">
		    <a href="http://www.joshevansart.com" class="prjctBtnLft" target="blank" title="Josh Evans Portfolio">Launch Site</a>
	    </div>
	    <div class="btnWrpr">
		    <a href="https://bitbucket.org/bkevans/joshevansart.v2" class="prjctBtnRgt" target="blank" title="Project Source Files">View Source</a>
	    </div> -->
    <p>This project is currently in progress, and is a completely new site design for Wilmington, NC surfboard shaper Jimmy Keith. Jimmy makes some of the nicest surfboards in the southeast, and needs a website to reflect the quality of his craftsmanship. This site's design has a new look and feel, is completely responsive to screen resolution, and features subtle animations and interactions. My projected date of completion is June 17, 2016, at which point I will migrate the completed source files over to Jimmy's server and perform site maintenance on an as needed basis. The images below show the index and contact pages in their current in-process state.</p>
   	<p>I am writing the HTML, Sass, and Javascript for this site in Sublime Text 3 from scratch, using LibSass and Grunt to compile my minified CSS and JS files. Image sizing and editing is being done in Photoshop CC, and I am rebuilding current logos as SVGs in Illustrator CC. Git is being used for version control via terminal, connecting with a bitbucket repository for storage redundancy and history tracking. The images used in the mockup are for development purposes only and will not be used in the final site.</p>
	<div class="prjctImgWrap"><img src="../img/project_img/jk_home.jpg" /></div>
	<div class="prjctImgWrap"><img src="../img/project_img/jk_contact.jpg" /></div>
</div>