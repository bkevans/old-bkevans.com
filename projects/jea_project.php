<div class="popup_project wow zoomIn">
	<img src="../img/jea_mockup.jpg" />
    <h1>Josh Evans Art</h1>
    <h2>Web Portfolio | Responsive Design</h2>
	    <div class="btnWrpr">
		    <a href="https://bitbucket.org/bkevans/joshevansart.v2" class="prjctBtnLft" target="blank" title="Project Source Files">View Source</a>
	    </div>
	    <div class="btnWrpr">
		    <a href="http://www.joshevansart.com" class="prjctBtnRgt" target="blank" title="Josh Evans Portfolio">Launch Site</a>
	    </div>
    <p>Josh Evans Art is a portfolio webpage I designed and custom built for my brother Josh. The purpose of this site is to showcase some of Josh's work as a 3-D video game environment artist. With Josh's creative input, I designed the look and feel of the site, and the site was custom developed from scratch. This site is fully responsive to different viewport resolutions, and features responsive images along with a touch enabled image gallery.</p>
   	<p>I wrote the HTML, Sass, and Javascript for this site in Sublime Text 3, using LibSass and Grunt to compile my minified CSS and JS files. Image sizing was done in Photoshop CC, and I converted logos to SVG in Illustrator CC. Git was used for version control via terminal, connecting with a bitbucket repository for storage redundancy and history tracking. Google analytics is installed for site traffic information and insights.</p>
	<div class="prjctImgWrap"><img src="../img/project_img/jea_home.jpg" /></div>
	<div class="prjctImgWrap"><img src="../img/project_img/jea_bioescape.jpg" /></div>
	<div class="prjctImgWrap"><img src="../img/project_img/jea_gallery.jpg" /></div>
</div>