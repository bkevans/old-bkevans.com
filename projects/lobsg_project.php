<div class="popup_project wow zoomIn">
	<img src="../img/lobsg_mockup.jpg" />
    <h1>Live Oak Bank</h1>
    <h2>Style Guide | Web Based Resource</h2>
	    <!--<div class="btnWrpr">
		    <a href="https://bitbucket.org/bkevans/joshevansart.v2" class="prjctBtnRgt" target="blank" title="Project Source Files">View Source</a>
	    </div> -->
    	<div class="btnWrprSngl">
		    <a href="http://styleguide.liveoakbank.com/" class="prjctBtnSngl" target="blank" title="Live Oak Bank Style Guide">Launch Site</a>
	    </div>
    <p>I built this corporate style guide in collaboration with the Art Director for Live Oak Bank to serve as an internal branding resource for web and print media. It houses things like brand logos and presents usage requirements in an understandable way. The color scheme for the brochure site, liveoakbank.com is found here, along with some of the components used to build the site and a brief visual of the foundation grid system used for the website's layout.</p>
   	<p>I built this style guide using the Foundation 5 framework, making use of the grid system and the sticky top-bar components. Smooth scroll has been implemented on the navigation items, and the site is fully responsive. All styling for this site was done with Sass, and Git was used for version control and collaboration.</p>
	<div class="prjctImgWrap"><img src="../img/project_img/lob_sg.jpg" /></div>
</div>