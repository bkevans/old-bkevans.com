module.exports = function(grunt){

"use strict";
require("matchdep").filterDev("grunt-*").forEach(grunt.loadNpmTasks);

    grunt.initConfig({
        
        pkg: grunt.file.readJSON('package.json'),

        cssc: {
            build: {
                options: {
                    consolidateViaDeclarations: true,
                    consolidateViaSelectors:    true,
                    consolidateMediaQueries:    true
                },
                files: {
                    'build/css/master.css': 'build/css/master.css'
                }
            }
        },

        cssmin: {
            build: {
                src: 'build/css/master.css',
                dest: 'build/css/master.css'
            },
            build: {
                src: 'build/css/animate.min.css',
                dest: 'build/css/animate.min.css'
            }, 
        },

        sass: {
            build: {
                files: {
                    'build/css/master.css': 'assets/sass/master.scss',
                    'build/css/animate.min.css': 'assets/sass/animate.scss'
                }
            }
        },

		watch: {
            js: {
                files: ['assets/js/base.js'],
                tasks: ['uglify']
            },
            css: {
                files: ['assets/sass/**/*.scss'],
                tasks: ['buildcss']
            }
        },

        uglify: {
            build: {
                files: {
                    'build/js/main.js': ['assets/js/base.js']
                }
            }
        }
    });

    grunt.registerTask('default', []);
    grunt.registerTask('buildcss',  ['sass', 'cssc', 'cssmin']);

};